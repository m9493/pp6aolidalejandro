﻿using System;
namespace Dades
{
    /// <summary>
    /// Conjunto de métodos para entrada y salida de datos
    /// </summary>
    public class Consola
    {
        /// <summary>
        /// Introducir una opción y pasarla a minúscula
        /// </summary>
        /// <returns>
        /// string en minúscula
        /// </returns>
        public static string ReadOptionLowerCase()
        {
            string option;
            do
            {
                option = Console.ReadLine();
            } while ((option == null));
            return option.ToLower();
        }
        
        /// <summary>
        /// Mostrar una pregunta
        /// </summary>
        public static void WriteQuestion()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("\n\n ¿Que palabra quieres buscar? ");
            Console.ResetColor();
        }
        
        /// <summary>
        /// Mostrar el contenido del menu
        /// </summary>
        public static void ShowMenu()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("\n INDICE DE EJERCICIOS:" + 
                          "\n\n [1] Introduce una palabra para buscar y copiar" +
                          "\n [2] Crea una copia del archivo con el número de consonantes" +
                          "\n [3] Crea 3 archivos con las palabras que empiecen por [j,k,l]" +
                          "\n\n Introduce 'salir' para finalizar el programa" +
                          "\n\n Introduce un ejercicio: ");
            Console.ResetColor();
        }

        /// <summary>
        /// Console clear
        /// </summary>
        public static void Clear()
        {
            Console.Clear();
        }
        
        /// <summary>
        /// Read key + clear
        /// </summary>
        public static void KeyClear()
        {
            Console.ReadKey();
            Console.Clear();
        }
        
        /// <summary>
        /// Mostrar opcion incorrecta
        /// </summary>
        public static void WriteOpcionIncorrecta()
        {
            Console.WriteLine("Opción Incorrecta");
        }
    }
}