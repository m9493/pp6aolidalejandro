﻿using System;
using System.Collections.Generic;
using System.IO;
namespace Principal
{
    /// <summary>
    /// 
    /// </summary>
    public class Pp6
    {
        /// <summary>
        /// Método para leer un archivo e introducir su contenido en una lista
        /// </summary>
        /// <param name="path">
        /// string: Path del archivo a leer
        /// </param>
        /// <param name="palabra">
        /// string: palabra a buscar
        /// </param>
        /// <returns>
        /// Lista de strings
        /// </returns>
        public List<string> ReadFileToList(string path, string palabra)
        {
            var palabrasList = new List<string>();
            using (var sr = new StreamReader(path))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if (line.Contains(palabra))
                    {
                        palabrasList.Add(line); 
                    }
                }
            }
            return palabrasList;
        }
        
        /// <summary>
        /// Método para buscar palabras que comiencen por un caracter e introducir su contenido en una lista
        /// </summary>
        /// <param name="path">
        /// string: Path del archivo a leer
        /// </param>
        /// <param name="first">
        /// char: Primer caracter de la palabra
        /// </param>
        /// <returns>
        /// Lista de strings
        /// </returns>
        public List<string> ReadFileToList3(string path, char first)
        {
            var palabrasList = new List<string>();
            using (var sr = new StreamReader(path))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if (line[0] == first)
                    {
                        palabrasList.Add(line); 
                    }
                }
            }
            return palabrasList;
        }

        /// <summary>
        /// Método para leer un archivo y el número de consonantes por palabra, ádemas copiar su contenido en otro archivo
        /// </summary>
        /// <param name="path">
        /// string: Path del archivo a leer
        /// </param>
        public List<string> ReadFileAndConsonantsToList(string path)
        {
            var palabrasList = new List<string>();
            using (var sr = new StreamReader(path))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    int num = 0;
                    foreach (var c in line)
                    {
                        if (c != 'a' && c != 'e' && c != 'i' && c != 'o' && c != 'u')
                            num++;
                    }
                    palabrasList.Add( Convert.ToString(num) + " " + line);
                }
            }
            return palabrasList;
        }

        /// <summary>
        /// Método para escrbir en un archivo una lista de strings
        /// </summary>
        /// <param name="path">
        /// string: Path del archivo a leer
        /// </param>
        /// <param name="palabrasList">
        /// Lista de strings
        /// </param>
        public void WriteListToFile(string path, List<string> palabrasList)
        {
            NewFileText(path);
            using (var sw = new StreamWriter(path))
            {
                foreach (var palabra in palabrasList)
                {
                    sw.WriteLine(palabra);
                }
            }
        }

        private static void NewFileText(string newPath)
        {
            if (!File.Exists(newPath))
            {
                File.CreateText(newPath);
            }
        }
    }
}