using Dades;
using Principal;
namespace Proves
{
    /// <summary>
    /// 
    /// </summary>
    public class Prova
    {
        static void Main()
        {
            Menu();
        }

        /// <summary>
        /// Método para buscar una palabra en un archivo y guardarla en otro archivo de texto
        /// </summary>
        static void SearchWordsAndWriteProva1()
        {
            Pp6 pp6 = new Pp6();
            Consola.WriteQuestion();
            var palabrasList = pp6.ReadFileToList("paraules.txt", Consola.ReadOptionLowerCase());
            pp6.WriteListToFile("paraules-resultat.txt",palabrasList);
        }
        
        /// <summary>
        /// Método para copiar el contenido de un archivo y contar el número de consonantes
        /// </summary>
        static void ReadAndCopyFileProva2()
        {
            Pp6 pp6 = new Pp6();
            
            var palabrasList = pp6.ReadFileAndConsonantsToList("paraules.txt");
            pp6.WriteListToFile("paraules-consonantes.txt",palabrasList);
        }
        
        /// <summary>
        /// Método para buscar una palabra en un archivo y guardarla en otro archivo de texto
        /// </summary>
        static void SearchWordsAndWriteProva3()
        {
            Pp6 pp6 = new Pp6();
            pp6.WriteListToFile("paraules-j.txt",pp6.ReadFileToList3("paraules.txt", 'j'));
            pp6.WriteListToFile("paraules-k.txt",pp6.ReadFileToList3("paraules.txt", 'k'));
            pp6.WriteListToFile("paraules-l.txt",pp6.ReadFileToList3("paraules.txt", 'l'));
        }
        
        /// <summary>
        /// Método para ejecutar el menu
        /// </summary>
        static void Menu()
        {
            var menu = true;
            while (menu)
            {
                Consola.ShowMenu();
                var option = Consola.ReadOptionLowerCase();
                Consola.Clear();
                switch (option)
                {
                    case "1":
                        Prova.SearchWordsAndWriteProva1();
                        break;
                    case "2":
                        Prova.ReadAndCopyFileProva2();
                        break;
                    case "3":
                        Prova.SearchWordsAndWriteProva3();
                        break;
                    case "salir":
                        menu = false;
                        break;
                    default:
                        Consola.WriteOpcionIncorrecta();
                        break;
                }

                if (option != "salir")
                    Consola.KeyClear();
            }
        }
    }
}